import os
import time
import asyncio
import subprocess
import requests
from fastapi import Depends
from fastapi import FastAPI, Request, Form, HTTPException
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
import threading
#from mangum import Mangum

token = None
name = ''
count = 0
first_time = True
app = FastAPI()
#handler = Mangum(app)

dir_path = os.path.dirname(os.path.realpath('index.html'))
path = dir_path+'\index.html'
python_file = dir_path +'\app\main.py'
templates = Jinja2Templates(directory=f"{dir_path}")

# Initialize values for messages
vehicles_number = ''
vehicle_list = None
vehicle_list_ = 'None'
driving_hours_message = "No driving hours message"
overspeeding_message = "No overspeeding message"
hash_acceleration_message = "No harsh acceleration message"
night_driving_ban_message = "No night driving ban message"

def update_data(token):
    current_dir = os.getcwd()
    ext = 'Lib', 'app'
    filename = 'update.py'
    full_path = os.path.join(current_dir, *ext, filename)
    subprocess.call(['python',full_path, str(token)])

@app.get("/home", response_class=HTMLResponse)
async def home_page(request: Request):
    return templates.TemplateResponse("home.html", {"request": request})

@app.get("/", response_class=HTMLResponse)
async def read_form(request: Request):
    global count
    global first_time
    if token is None:
        return RedirectResponse(url='/home')

    if count == 0:
        thread = threading.Thread(target=update_data, args=(token,))
        thread.start()
        count += 1

    if vehicle_list != None and first_time == True:
        first_time = False
        print('redirecting')
        return RedirectResponse(url='/', status_code=303)

    return templates.TemplateResponse("index.html", {
        "request": request,
        "name": name,
        'vehicles_number': vehicles_number,
        'vehicle_list': vehicle_list,
        "driving_hours_message": driving_hours_message,
        "overspeeding_message": overspeeding_message,
        "hash_acceleration_message": hash_acceleration_message,
        "night_driving_ban_message": night_driving_ban_message
    })

@app.get("/login", response_class=HTMLResponse)
async def login_form(request: Request):
    return templates.TemplateResponse("login.html", {"request": request})

@app.post('/login')
async def login(request: Request, email: str = Form(...), password: str = Form(...)):
    global token
    global name
    web = 'https://service.packet-v.com/api/login'
    payload = {
        "email": email,
        "password": password
    }

    try:
        response = requests.post(web, json=payload)
        data = response.json()
        token = data.get('user_api_hash')

        if token is None:
            error_message = 'Incorrect email or password'
            return templates.TemplateResponse("login.html", {"request": request, "error_message": error_message})
        name = email.split('@')[0].replace('.', ' ')
        # Redirect to the index page after successful login
        return RedirectResponse(url="/", status_code=303)

    except requests.exceptions.RequestException:
        network_error = True
        return templates.TemplateResponse("login.html", {"request": request, "network_error": network_error})

@app.post("/update_driving_hours")
async def update_driving_hours(driving_hours: dict):
    global driving_hours_message
    driving_hours_message = driving_hours.get("message")
    return {"message": driving_hours_message }

@app.post("/update_overspeeding")
async def update_overspeeding(overspeeding: dict):
    global overspeeding_message
    overspeeding_message = overspeeding.get("message")
    return {"message": overspeeding_message }

@app.post("/update_hash_acceleration")
async def update_hash_acceleration(hash_acceleration: dict):
    global hash_acceleration_message
    hash_acceleration_message = hash_acceleration.get( "message")
    return { "message": hash_acceleration_message }

@app.post("/update_night_driving_ban")
async def update_overspeeding(nightdriving: dict):
    global night_driving_ban_message
    night_driving_ban_message = nightdriving.get("message")
    return {"message": night_driving_ban_message}

@app.post("/update_number_of_vehicles")
async def update_number_of_vehicles(number_of_vehicles: dict):
    global vehicles_number
    vehicles_number = number_of_vehicles.get('message')
    return {'message':vehicles_number}

@app.post("/update_vehicle_list")
async def update_vehicle_list(list_of_vehicles: dict):
    global vehicle_list
    vehicle_list = list_of_vehicles.get('message')
    return {'message': vehicle_list}

@app.post("/update_vehicle_list_")
async def update_vehicle_list(list_of_vehicles_: dict):

    global vehicle_list_
    vehicle_list_ = list_of_vehicles_.get('message')
    return {'message': vehicle_list_}
