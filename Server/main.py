from fastapi import FastAPI

app = FastAPI()

jsonData = None

@app.put("/update-json")
def update_json(new_json_data: dict):
    global jsonData
    jsonData = new_json_data
    return {"message": "JSON data updated successfully"}

@app.get("/get-json")
def get_json():
    return jsonData
