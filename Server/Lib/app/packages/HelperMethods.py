import time
from datetime import datetime,timedelta


class HelperMethods:
    def convert_to_seconds(self,time_str):
        """Get seconds from time."""
        h, m, s = time_str.split(':')
        return int(h) * 3600 + int(m) * 60 + int(s)

    def get_current_time(self):
        zm_timezone = timedelta(hours=3)
        zm_time = datetime.utcnow() + zm_timezone
        zm_time = zm_time.strftime("%H:%M:%S")
        #convert hours,minutes to just seconds
        current_time = self.convert_to_seconds(zm_time)
        return current_time

    def get_time_and_date(self):
        zm_timezone = timedelta(hours=3)
        zm_time = datetime.utcnow() + zm_timezone
        return zm_time
