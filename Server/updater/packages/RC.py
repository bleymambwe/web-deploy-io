import requests

class RoadClassifier:
    def __init__(self):
        pass
    def address(self,latitude,longitude):
        url = f"https://nominatim.openstreetmap.org/reverse?lat={latitude}&lon={longitude}&format=json"
        try:
            response = requests.get(url, timeout=3)
            response = response.json()
            return response['address']['road']
        except:
            return '  '


    def classify(self,lat,lon):
        road_name = self.get_road_osm(lat,lon)
        info = self.get_tags_overpass(lat,lon)
        classification = self.check_database(road_name)

        if classification == True or info[1] == 'highway' and info[2] == 'primary' or 'alphalt':
            return 'Highway'
        elif classification == False and info[1] == 'highway' and info[2] == 'primary' or 'alphalt':
            return 'Highway'
        elif classification == True or info[1] == 'highway' and info[2] != 'primary' or 'alphalt':
            return 'Dust Road'
        else:
            return 'Build-up Area'


    def get_road_osm(self,latitude,longitude):
        url = f"https://nominatim.openstreetmap.org/reverse?lat={latitude}&lon={longitude}&format=json"
        try:
            response = requests.get(url, timeout=3)
            response = response.json()
            return response['address']['road']
        except:
            return 'Connection Failed'

    def database_1(self,road_name):
        Roads = {'Great North Road','Great East Road', 'Mongu-Kalabo Road', 'Lumumba Road', 'Mumbwa-Kasempa Road', 'Chingola-Solwezi Road','Serenje-Nakonde Road','Sesheke-Livingstone Road','Kalomo-Choma Road','Petauke-Port Herald Road','Luanshya-Masaiti Road','Mpongwe-Ndola Road','Solwezi-Mwinilunga Road','Kabwe-Kapiri Mposhi Road',
             'Luangwa Bridge-Ch Road','Tanzam Highway Road','Mumbwa Road',
             'Walvis Bay-Ndola-Lubumbashi Development Road','Cairo Road','Congo Pedicle road','Lusaka–Mongu Road','Lusaka–Livingstone Road'}

        return road_name in Roads

    def database_2(self,road_name):
        first_letter = {'D','M','T','R'}
        for i in range(len(road_name)):

            if road_name[i] in first_letter:
                try:
                    num = int(road_name[i+1])
                    if road_name[i] == 'D' and num >= 1 and num <= 817:
                        return True
                    elif road_name[i] == 'M' and num >= 1 and num <= 20:
                        return True
                    elif road_name[i] == 'T' and num >= 1 and num <= 6:
                        return True
                    else:
                        return False
                except:
                    try:
                        num = int(road_name[i+2])
                        if road_name[i] == 'R' and road_name[i+1] == 'D' or road_name[i] == 'R' and road_name[i+1] == 'N':
                            return True
                    except:
                        return False
            else:
                return False

    def check_database(self,road_name):
        return self.database_1(road_name) or self.database_2(road_name)


    def get_tags_overpass(self,latitude,longitude):

        overpass_query = f"""
        [out:json];
        (
            way(around:100, {latitude}, {longitude})["highway"];
            node(around:100, {latitude}, {longitude})["place"];
        );
        out center;
            """

        # Define the URL for the Overpass API
        overpass_url = "https://overpass-api.de/api/interpreter"

        # Make a POST request to the Overpass API to execute the query and retrieve the data
        try:
            response = requests.post(overpass_url, data=overpass_query)
        # Parse the JSON response and extract the relevant data
            data = response.json()

            road_type = data['elements'][0]['tags']['highway']
            road_surface = data['elements'][0]['tags']['surface']
            road_name = data['elements'][0]['tags']['ref']

            if len(data['elements'][0]['tags']) >= 5:
                if road_surface == 'asphalt' or 'primary' or 'track':
                    road_surface = 'highway'
                elif road_type == 'residential' :
                    road_type ='Built-Up Area'
                elif road_surface == 'dirt':
                    road_surface = 'Dust Road'
                else:
                    return road_name,road_surface,road_type
        except:
            return 'Connection Failed'

        return road_name, road_surface,road_type
